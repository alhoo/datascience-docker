# Start a local continer registry

    netstat -nlp|grep -i 5000
    ls -lhtra /proc/18126/cwd
    docker container rename registry registry_old
    docker run -d -p 5000:5000 --restart=always --name registry registry:2

# Publish locally the docker-composed service

    docker build -f Dockerfile_nogpu -t datascience_nogpu .
    docker tag datascience_nogpu localhost:5000/datascience_nogpu
    docker push localhost:5000/datascience_nogpu
    kompose -f docker-compose-nogpu-nobuild.yml up

# Show kubernetes services

    kubectl get deployment,svc,pods,pvc

# Open the kubernetes notebook

    minikube service notebook
