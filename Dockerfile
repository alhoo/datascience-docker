FROM ubuntu:18.04

# This image contains machine learning and datascience tools for
# speech, audio, video and general data pipeline creation. Use this
# to develop the Library and services around it.

WORKDIR /root/

# Add libcuda dummy dependency
ADD control .
RUN apt-get update && \
	apt-get install --yes equivs && \
	equivs-build control && \
	dpkg -i libcuda1-dummy_10.0_all.deb && \
	rm control libcuda1-dummy_10.0_all.deb && \
	apt-get remove --yes --purge --autoremove equivs && \
	rm -rf /var/lib/apt/lists/*

# Setup Lambda repository
ADD lambda.gpg .
RUN apt-get update && \
	apt-get install --yes gnupg && \
	apt-key add lambda.gpg && \
	rm lambda.gpg && \
	echo "deb http://archive.lambdalabs.com/ubuntu bionic main" > /etc/apt/sources.list.d/lambda.list && \
	echo "Acquire::http::Pipeline-Depth 0;" > /etc/apt/apt.conf.d/99fixbadproxy && \
  echo "Acquire::http::No-Cache true;"   >> /etc/apt/apt.conf.d/99fixbadproxy && \
  echo "Acquire::BrokenProxy    true;"   >> /etc/apt/apt.conf.d/99fixbadproxy && \
	echo "Package: *" > /etc/apt/preferences.d/lambda && \
	echo "Pin: origin archive.lambdalabs.com" >> /etc/apt/preferences.d/lambda && \
	echo "Pin-Priority: 1001" >> /etc/apt/preferences.d/lambda && \
	echo "cudnn cudnn/license_preseed select ACCEPT" | debconf-set-selections && \
	apt-get update && \
	DEBIAN_FRONTEND=noninteractive \
		apt-get install --no-install-recommends --yes lambda-stack-cuda lambda-server && \
	rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install --yes python3-dev

RUN pip3 install --upgrade \
    requests \
    matplotlib \
    ipykernel \
    jupyter \
    h5py \
    dask \
    distributed \
    joblib

RUN pip3 install --upgrade\
    redis-py-cluster \
    kafka-python \
    kazoo

RUN pip3 install --upgrade\
    torch \
    spacy \
    bokeh \
    lime \
    python_speech_features \
    librosa pysoundfile

RUN pip3 install --upgrade\
    flask PyYAML pycrypto lxml xlrd

RUN apt-get update && apt-get install --yes ffmpeg libsndfile1 sox libsox-fmt-all

RUN python3 -m ipykernel install --name datascience

RUN pip3 install git+https://github.com/alhoo/sacred.git
RUN python3 -m spacy download en_core_web_sm

EXPOSE 8888 6006
# VOLUME /Library
RUN useradd -d /home/ubuntu -ms /bin/bash -g root -G sudo -p ubuntu ubuntu
RUN mkdir /home/ubuntu/work  && chown ubuntu /home/ubuntu/work
RUN mkdir /home/ubuntu/data  && chown ubuntu /home/ubuntu/data
ENV NAME ubuntu
ENV HOME /home/ubuntu
USER ubuntu

VOLUME /home/ubuntu/work
VOLUME /home/ubuntu/data
WORKDIR "/home/ubuntu"
# VOLUME /notebooks
# WORKDIR "/notebooks"

COPY jupyter_notebook_config.py /home/ubuntu/.jupyter/
ENV NOTEBOOK_PASSWORD 7dd0ed2b378d9f39ce2273a98ebb39dc3c30af6e77c17f56
ENV LD_LIBRARY_PATH /usr/lib/x86_64-linux-gnu

# Setup for nvidia-docker
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=10.0"

CMD jupyter notebook --allow-root --ip='0.0.0.0' --port=8888 --no-browser
