#docker build -t datascience2 . && nvidia-docker run --rm -it -p 9000:8888 --name ds datascience2

NOTEBOOK="/home/lasse/Desktop"
DATA=$(echo /media/lasse/853bf813-fe58-4786-9d4a-186d28bf36fe*/Data|awk '{print $1}'|head -n 1)
LIBRARY="/home/lasse/Projects/Library"


# sudo rmmod nvidia_uvm ; sudo modprobe nvidia_uvm
docker build -t datascience . && nvidia-docker run -p 6006:6006 -p 9999:8888 -v ${LIBRARY}:/Library -v ${DATA}:/var/data:ro -v ${NOTEBOOK}:/notebooks --ipc=host datascience:latest || \
   sudo rmmod nvidia_uvm ; sudo modprobe nvidia_uvm && \
   docker build -t datascience . && nvidia-docker run -p 6006:6006 -p 9999:8888 -v ${DATA}:/var/data:ro -v ${NOTEBOOK}:/notebooks --ipc=host datascience:latest
  

